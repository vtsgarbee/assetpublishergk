from enum import Enum

class dataKeyMappings(Enum):

    ovrBake = 0
    cutInBakeOvr = 1
    cutOutBakeOvr = 2

    ovrPreview = 3
    cutInPreviewOvr = 4
    cutOutPreviewOvr = 5

    bakeOutput = 6
    previewOutput = 7

    mblurduration = 8
    dofpasses = 9

# ----------------------------------------------------------------------------------------------------------------------

class DNode(object):
    code = ""
    children = {}
    properties = {}
    misc = {}
    parent = None

    def __init__(self, code = "%UNTITLED%", parent=None, properties=None):

        self.code = code
        self.parent = parent

        if self.parent is not None:
            res = self.parent.AddChild(code, self)
            if not res:
                return None

        self.children = {}

        if properties is None:
            properties = {}

        self.properties = properties
        self.misc = {}

    def SetParent(self, parent, override = False):

        if parent is None:
            return False

        res = parent.AddChild(self.code, self, override)

        if not res:
            return False

        self.parent = parent
        return True

    def Rename(self, newCode):

        if self.parent is None:
            return False

        res = self.DeleteNode()

        if not res:
            return False

        self.code = newCode
        self.parent.AddChild(newCode, self, True)

        return True

    def AddChild(self, code, node, override = False):

        if not override:
            if code in self.children:
                return False

        self.children[code] = node
        return True

    def DeleteNode(self):

        if self.parent is None:
            return False

        res = self.parent.DeleteChild(self.code)
        return res

    def DeleteChild(self, code):
        if code in self.children:
            del self.children[code]
            return True

        return False

    def DeleteAllChildren(self):
        self.children = {}
        return True

    def GetChild(self, code):
        if code in self.children:
            return self.children[code]

        return None

    def AddProperty(self, code, value):
        self.properties[code] = value
        return True

    def HasProperty(self, code):
        if code in self.properties:
            return True
        else:
            return False

    def SetProperty(self, code, value):

        if self.HasProperty(code):
            self.properties[code] = value
            return True
        else:
            return False

    def GetProperty(self, code):
        if code in self.properties:
            return self.properties[code]

        return None

    def DeleteProperty(self, code):
        if code in self.properties:
            del self.properties[code]
            return True

    def SetMisc(self, code, value):
        self.misc[code] = value
        return True

    def GetMisc(self, code):
        if code in self.misc:
            return self.misc[code]

        return None

    def SetOverride(self, code, value):
        self.overrides[code] = value
        return True

    def HasOverride(self, code):
        if code in self.overrides:
            return True
        else:
            return False

    def GetOverride(self, code):
        if code in self.overrides:
            return self.overrides[code]

        return None

    def DeleteOverride(self, code):
        if code in self.overrides:
            del self.overrides[code]
            return True

        return False

    def ApplyTypeToBranch(self, type):

        self.type = type

        for c in self.children:
            self.children[c].ApplyTypeToBranch(type)

    def RecursiveSearch(self, code):

        if (str(self.code) == str(code)):
            return self

        for c in self.children:
            res = self.children[c].RecursiveSearch(code)
            if (res != None):
                return res

        return None