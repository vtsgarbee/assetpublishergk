import os
import sys
import pymxs

# mainPath = "C:\\realtimeuk\\toolbox\\tools\\HelloShotgun\\CameraPublishingTool_files"

# DEV ------------------------------------------------------------------------------------------------------------------

mainPath = os.path.abspath(r"C:\Users\federico\Desktop\Python\AssetPublisherGK")
eggPath = os.path.abspath(r"C:\Users\Federico\Desktop\Python\debug-eggs\pydevd-pycharm.egg")

if not sys.path.__contains__(eggPath):
    sys.path.append(eggPath)

import pydevd_pycharm

print ("DBG > Connecting to Debug... press F9 when ready")
pydevd_pycharm.settrace('localhost', port=72)

pymxs.runtime.execute('global APUBPATH; APUBPATH=@"' + mainPath + '"')

if not sys.path.__contains__(mainPath):
    sys.path.append(mainPath)

import AP_Mvc
reload(AP_Mvc)

import AP_Sanitizer
reload(AP_Sanitizer)

import UncModule
reload(UncModule)

import AP_HelperFunctions
reload(AP_HelperFunctions)

import AP_AssetPublisher
reload(AP_AssetPublisher)

import AP_MaxBridge
reload(AP_MaxBridge)

import AP_JsonModule
reload(AP_JsonModule)

AP_JsonModule.JsonParser()
AP_MaxBridge.SceneConnection()
UncModule.UncHandler.instance()

AP_AssetPublisher.Publisher.instance()
cpGui = AP_Mvc.APubGUI()
