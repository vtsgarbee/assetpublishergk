import pymxs
import AP_MaxBridge


class JsonParser:

    instance = None

    def __init__(self):
        self.SetInstance(self)

    @staticmethod
    def SetInstance(inst):
        JsonParser.instance = inst

    @staticmethod
    def Instance():
        return JsonParser.instance

    #Writes the data to the max object. Converts from proper json to a max compatible single quoted string
    def SetData(self, data, field):

        maxString = data.replace('"', "'")

        with pymxs.undo(False):
            AP_MaxBridge.SceneConnection.Instance().WriteToNode(maxString, field)


    #Gets the data from the max object. Converts from the max compatible single quoted string to proper json
    def GetData(self, field):

        data = AP_MaxBridge.SceneConnection.Instance().ReadFromNode(field)
        data = data.replace("'", '"')

        return data