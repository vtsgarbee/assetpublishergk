import pymxs
import pathlib
import AP_HelperFunctions

try:
    from PySide2 import QtCore
except:
    from PySide import QtCore

rt = pymxs.runtime
lm = rt.layerManager

class Publisher(QtCore.QObject):

    inst = None
    cameras = {}
    bakePath = None
    renderPath = None
    _ABORTED = False

    opNumFound = QtCore.Signal(object)
    opDone = QtCore.Signal(object)
    done = QtCore.Signal()

    @staticmethod
    def instance():

        if Publisher.inst is None:
            Publisher.inst = Publisher()

        return Publisher.inst

    def __init__(self):
        super(Publisher, self).__init__()


    def publish(self, layerName, assetName, completePath):

        layerName = "!PUBLISHED"

        print "Holding max file..."
        rt.holdMaxFile(quiet=True)

        nodes = AP_HelperFunctions.recursiveGetLayerNodes(layerName)

        rt.select(rt.objects)
        rt.deselect(nodes)
        rt.delete(rt.selection)

        print "Setting up layers..."
        AP_HelperFunctions.cleanEmptyLayers()

        pLayer = lm.getLayerFromName(layerName)

        if pLayer is None:
            print "Cannot find the associated layer"
            rt.fetchMaxFile(quiet=True)
            return False

        res = pLayer.setName(assetName)

        if not res:
            print "Something went wrong while setting up the layers"
            rt.fetchMaxFile(quiet=True)
            return False

        completePath = pathlib.Path(completePath)
        outputFolder = completePath.parent

        print "Checking output folders..."

        if not outputFolder.exists():
            pathlib.Path.mkdir(outputFolder, parents=True)

        textureFolder = outputFolder.joinpath("Maps")

        if not textureFolder.exists():
            pathlib.Path.mkdir(textureFolder, parents=True)


        if not outputFolder.exists() or not textureFolder.exists():
            print "Cannot create the necessary folders"
            rt.fetchMaxFile(quiet=True)
            return False

        print "Collecting all dependencies..."

        textures, materials, others = AP_HelperFunctions.getDependenciesByType(nodes)

        resources = []

        print "Collecting all the texture resources..."

        for t in textures:
            if rt.classOf(t) == rt.VRayHDRI:
                path = t.HDRIMapName
                rList = AP_HelperFunctions.collectResourcesForMap(path)
                resources = resources + rList

        print "Printing resources..."
        for r in resources:
            print r

        print "Saving asset to %s" % str(completePath)
        rt.saveNodes(rt.objects, str(completePath), quiet=True)

        print "Done. Fetching max file..."
        rt.fetchMaxFile(quiet=True)

        print "Done"
        self.done.emit()
        return True
