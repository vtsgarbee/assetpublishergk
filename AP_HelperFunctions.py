import pymxs
import pathlib

rt = pymxs.runtime
lm = rt.layerManager

# HELPER FNs -----------------------------------------------------------------------------------------------------------

getNodesFn = """
layer = layerManager.getLayerFromName("%s")
layerObjs = #()
layer.nodes(&layerObjs)
"""

def getNodesFromLayer(layerName):
    mxs = (getNodesFn % layerName)
    rt.execute(mxs)
    return list(rt.layerObjs)

def recursiveGetLayerNodes(layerName):

    layer = lm.getLayerFromName(layerName)

    if layer is None:
        return []

    nodes = getNodesFromLayer(layerName)

    for i in range(layer.getNumChildren()):
        childLayer = layer.getChild(i+1)
        nodes.extend(recursiveGetLayerNodes(childLayer.name))

    return nodes

def recursiveGetNodeDependencies(node, collectedHandles=set([])):

    deps = list(rt.refs.dependsOn(node))
    depsCopy = list(deps)

    for d in depsCopy:

        h = rt.GetHandleByAnim(d)

        if h in collectedHandles:
            continue

        if not rt.refs.dependencyLoopTest(d, node):
            collectedHandles.add(h)
            deps = deps + recursiveGetNodeDependencies(d)

    return deps

def getDependenciesByType(nodes):

    dependencies = []
    materials = []
    textures = []
    others = []
    collectedHandles = set([])

    print "Collecting dependencies..."

    for n in nodes:
        dependencies = dependencies + recursiveGetNodeDependencies(n, collectedHandles)

    print "Filtering dependencies..."

    for d in dependencies:

        sc = rt.superClassOf(d)

        if sc == rt.material:
            materials.append(d)
        elif sc == rt.textureMap:
            textures.append(d)
        else:
            others.append(d)

    # print len(dependencies)
    # print len(materials)
    # print len(textures)
    # print len(others)

    print "FOUND: %d D, %d T, %d M, %d O" %(len(dependencies), len(textures), len(materials), len(others))

    return textures, materials, others

def collectResourcesForMap(filename):

    if filename is None:
        return []

    file = pathlib.Path(filename)

    name = str(file.stem).lower()
    ext = str(file.suffix).lower()

    if ext == ".tx":
        # if it's tx, we grab all the extensions
        ext = ""

    if "<udim>" in name:
        # if it's UDIM, we change the wildcard so that the whole series will be globbed
        name = name.replace("<udim>", "")

    pattern = "%s*%s" % (name, ext)
    res = file.parent.glob(pattern)

    return list(res)


def cleanEmptyLayers():

    layerCount = lm.count

    for maxIter in range(15):

        for i in range(lm.count):
            layer = lm.getLayer(i)

            if layer is not None:
                lm.deleteLayerByName(layer.name)

        if lm.count == layerCount:
            return True

    return False