import os
import MaxPlus
import pymxs
import AP_Sanitizer
import re
import UncModule
import pathlib
import AP_AssetPublisher

import AP_CustomData
import AP_JsonModule
import jsonpickle
import AP_MaxBridge

try:
    import PySide2
    from PySide2.QtCore import *
    from PySide2.QtGui import *
    from PySide2.QtWidgets import *

except:
    import PySide
    from PySide.QtCore import *
    from PySide.QtGui import *


mainPath = pymxs.runtime.execute("APUBPATH")
apubUiType, apubBaseType = MaxPlus.LoadUiType(os.path.join(mainPath, "apub.ui"))
apubHelpUiType, apubHelpBaseType = MaxPlus.LoadUiType(os.path.join(mainPath, "apubHelp.ui"))

REFRESH_ICON = QIcon(os.path.join(mainPath, "res\\refresh.png"))
HELP_ICON = QIcon(os.path.join(mainPath, "res\\help.png"))
FOLDER_ICON = QIcon(os.path.join(mainPath, "res\\folder.png"))
WARNING_ICON = QIcon(os.path.join(mainPath, "res\\warning.png"))
ERROR_ICON = QIcon(os.path.join(mainPath, "res\\error.png"))
MESSAGE_ICON = QIcon(os.path.join(mainPath, "res\\message.png"))
# BAKE_ICON = QIcon(os.path.join(mainPath, "RES\\bake.png"))
# RENDER_ICON = QIcon(os.path.join(mainPath, "RES\\render.png"))
# NOFX_ICON = QIcon(os.path.join(mainPath, "RES\\nofx.png"))
# MBLUR_ICON = QIcon(os.path.join(mainPath, "RES\\mblur.png"))
# DOF_ICON = QIcon(os.path.join(mainPath, "RES\\dof.png"))
# CAM_ICON = QIcon(os.path.join(mainPath, "RES\\camera.png"))
# NO_CAM_ICON = QIcon(os.path.join(mainPath, "RES\\nocamera.png"))
# CAM_DISABLED_ICON = QIcon(os.path.join(mainPath, "RES\\cameraNotValid.png"))
# CLEAN_ICON = QIcon(os.path.join(mainPath, "RES\\clean.png"))
# HELP_ICON = QIcon(os.path.join(mainPath, "RES\\help.png"))

# ----------------------------------------------------------------------------------------------------------------------


# --------------------------------------------------------


class ChecksModel(QAbstractTableModel):

    def __init__(self):

        QAbstractTableModel.__init__(self)

        # Init keys ------------------------

        self.keys = ["category", "code", "issue", "info", "fix"]
        self.mappings = {}

        for i, key in enumerate(self.keys):
            self.mappings[key] = i

        self.checkData = {}
    #
    #     try:
    #
    #         jString = AP_JsonModule.JsonParser.Instance().GetData("cameras")
    #         self.assetData = jsonpickle.decode(jString)
    #         self.RefreshCameras()
    #         print str(len(self.assetData)) + " cameras retrieved from json"
    #
    #     except Exception as e:
    #
    #         print "Cannot retrieve Camera data from json file:"
    #         print e
    #         self.assetData = []
    #         self.RefreshCameras()
    #
    # def writeToJson(self):
    #
    #     j = jsonpickle.encode(self.assetData)
    #     AP_JsonModule.JsonParser.Instance().SetData(j, "cameras")
    #
    #     return True

    def rowCount(self, in_index=None):
        return len(self.checkData)

    def columnCount(self, in_index=None):
        return len(self.keys)

    def data(self, index, role):

        key = self.keys[index.column()]
        checkCode = self.checkData.keys()[index.row()]
        check = self.checkData[checkCode]

        if role == Qt.DisplayRole or role == Qt.EditRole:

            if key == "code":
                return check.code

            if key == "issue":
                return check.msg

            if key == "info":
                return check.info()

            if key == "category":
                return check.category

        if role == Qt.DecorationRole and (key == "issue"):

            if check.priority == 1:
                return WARNING_ICON
            elif check.priority ==2:
                return ERROR_ICON
            else:
                return MESSAGE_ICON

    def headerData(self, section, orientation, role):

        if role == Qt.DisplayRole:

            if orientation == Qt.Horizontal:
                return self.keys[section].capitalize()

            if orientation == Qt.Vertical:
                return section+1

    def setData(self, index, value, role):
    #
    #     key = AP_CustomData.kesMappings(index.column()).name
    #
    #     if role == Qt.EditRole and index.isValid:
    #
    #         self.assetData[index.row()].SetProperty(key, value)
    #         self.dataChanged.emit(index, index)
    #         return True
    #
    #     return False

        return True

    def flags(self, index):

        qFlags = super(self.__class__, self).flags(index)
        return qFlags


    # CUSTOM MODEL METHODS ------------------------------------------------------------------------------------------------

    def checkEverything(self):

        self.beginResetModel()

        sanitizer = AP_Sanitizer.Sanitizer.instance()
        self.checkData = sanitizer.runChecks()

        for eCode in self.checkData:

            e = self.checkData[eCode]
            print e.code, e.msg

        self.endResetModel()


# Init keys ------------------------

assetDataKeys = ["rootFolder", "assetName", "variant", "version", "completeAssetName", "assetPath"]
assetDataMappings = {}

for index, key in enumerate(assetDataKeys):
    assetDataMappings[key] = index

# --------------------------------------------------------

class AssetDataModel(QAbstractListModel):

    assetData = {}

    def __init__(self):

        QAbstractListModel.__init__(self)

        try:

            jString = AP_JsonModule.JsonParser.Instance().GetData("assetData")
            self.assetData = jsonpickle.decode(jString)
            print "Publishing data retrieved from json"

        except Exception as e:

            print "Cannot retrieve data from json:"#
            print str(e)

            self.assetData = {}

            self.assetData["rootFolder"] = "C:\\\\PublishedAssets"
            self.assetData["assetName"] = "PyDevTest"
            self.assetData["version"] = 1
            self.assetData["variant"] = ""
            self.computeDetails()


    def writeToJson(self):

        j = jsonpickle.encode(self.assetData)
        AP_JsonModule.JsonParser.Instance().SetData(j, "assetData")

        return True

    def rowCount(self, in_index = None):
        return len(self.assetData)

    def data(self, index, role):

        try:
            key = assetDataKeys[index.row()]
            value = self.assetData[key]

            if type(value) == str:
                value = value.replace("\\\\", "\\")

            if role == Qt.DisplayRole or role == Qt.EditRole:
                return value

        except Exception as e:
            print "Something went wrong while retrieving data:"
            print e


    def setData(self, index, value, role):

        key = assetDataKeys[index.row()]

        if key == "assetName" or key == "variant":
            value = re.sub(r'[^\w]', '', value)    #Sanitize: only letters and underscores

        elif key == "rootFolder":
            value = re.sub(r'[^a-zA-Z0-9_\\/:].', '', value)     # Sanitize: only letters, underscores, slashes

            uncPath = UncModule.UncHandler.instance().convertToUnc(value)

            if uncPath is not None:
                value = str(uncPath)
            else:
                value = str(pathlib.Path(value))

        if type(value) == str:
            value = value.replace("\\", "\\\\")

        if role == Qt.EditRole and index.isValid:
            self.assetData[key] = value
            self.dataChanged.emit(index, index)

            self.computeDetails()

            return True

        return False

    def computeDetails(self):

        assetName = self.assetData["assetName"]
        rootFolder = self.assetData["rootFolder"]
        version = str(self.assetData["version"]).zfill(3)
        variant = self.assetData["variant"]

        if variant == "":
            completeAssetName = assetName
        else:
            completeAssetName = ("%s_%s" %(assetName, variant))

        completeAssetName = completeAssetName.replace("\\", "\\\\")
        self.assetData["completeAssetName"] = completeAssetName

        versionedName = ("%s_V%s.max" %(completeAssetName, version))

        assetPath = pathlib.Path(rootFolder)
        assetPath = assetPath.joinpath(assetName)
        assetPath = assetPath.joinpath(versionedName)
        assetPath = str(assetPath).replace("\\", "\\\\")

        self.assetData["assetPath"] = assetPath

        index = self.createIndex(assetDataMappings["assetPath"], 0)
        self.dataChanged.emit(index, index)

        index = self.createIndex(assetDataMappings["completeAssetName"], 0)
        self.dataChanged.emit(index, index)


# ----------------------------------------------------------------------------------------------------------------------


class APubHelpGUI(apubHelpUiType, apubHelpBaseType):

    def __init__(self):

        apubHelpUiType.__init__(self)
        apubHelpBaseType.__init__(self)
        self.setupUi(self)
        self.show()

class APubGUI(apubUiType, apubBaseType):

    def __init__(self):

        self.SetInstance(self)

        apubBaseType.__init__(self)
        apubUiType.__init__(self)
        self.setupUi(self)

        # CHECKS MVC ------------

        self.checksModel = ChecksModel()
        self.checksTable.setModel(self.checksModel)

        header = self.checksTable.horizontalHeader()

        # try:
        header.setResizeMode(0, QHeaderView.ResizeToContents)
        header.setResizeMode(1, QHeaderView.ResizeToContents)
        header.setResizeMode(2, QHeaderView.ResizeToContents)
        header.setResizeMode(3, QHeaderView.Stretch)
        header.setResizeMode(4, QHeaderView.Fixed)
        # except:
        #     header.setSectionResizeMode(0, QHeaderView.Fixed)
        #     header.setSectionResizeMode(1, QHeaderView.ResizeToContents)
        #     header.setSectionResizeMode(2, QHeaderView.Stretch)
        #     header.setSectionResizeMode(3, QHeaderView.Fixed)

        # header.resizeSection(0, 550)
        header.resizeSection(4, 100)
        self.checksTable.setColumnHidden(1, True)

        # PUBLISHING MVC ------------

        self.assetDataModel = AssetDataModel()

        mapper = QDataWidgetMapper(self)
        mapper.setOrientation(Qt.Orientation.Vertical)
        mapper.setModel(self.assetDataModel)

        mapper.addMapping(self.versionSpin, assetDataMappings["version"])
        mapper.addMapping(self.assetNameLabel, assetDataMappings["assetName"])
        mapper.addMapping(self.variantLabel, assetDataMappings["variant"])
        mapper.addMapping(self.rootFolderLabel, assetDataMappings["rootFolder"])

        mapper.addMapping(self.completeAssetNameLabel, assetDataMappings["completeAssetName"], "text")
        mapper.addMapping(self.assetPathLabel, assetDataMappings["assetPath"], "text")

        mapper.toFirst()

        # CONNECTIONS ------------

        self.helpButton.clicked.connect(self.ShowHelp)
        self.helpButton.setIcon(HELP_ICON)

        self.refreshButton.clicked.connect(lambda: self.checksModel.checkEverything())
        self.refreshButton.setIcon(REFRESH_ICON)

        self.browseButton.setIcon(FOLDER_ICON)

        self.goButton.clicked.connect(self.launchPublish)

        # FINAL GUI SETUP ------------

        try:
            self.setParent(MaxPlus.GetQMaxWindow())
        except:
            MaxPlus.AttachQWidgetToMax(self)

        self.setAttribute(Qt.WA_DeleteOnClose)
        self.setFocusPolicy(Qt.ClickFocus)

        self.show()


    @staticmethod
    def SetInstance(inst):
        APubGUI.instance = inst

    @staticmethod
    def Instance():
        return APubGUI.instance

    def closeEvent(self, event):

        # self.cameraModel.writeToJson()
        self.assetDataModel.writeToJson()
        event.accept()

    def launchPublish(self):

        p = AP_AssetPublisher.Publisher.instance()

        layerName = "!PUBLISHED"
        assetName = self.assetDataModel.assetData["completeAssetName"]
        assetPath = self.assetDataModel.assetData["assetPath"]

        p.publish(layerName, assetName, assetPath)

    def ShowHelp(self):

        self.help = APubHelpGUI()