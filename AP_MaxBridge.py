import MaxPlus
import re
import pymxs
import AP_CustomData

rt = pymxs.runtime

class SceneConnection:

    instance = None

    def __init__(self):

        self.InitNode()
        self.SetInstance(self)

    @staticmethod
    def SetInstance(inst):
        SceneConnection.instance = inst

    @staticmethod
    def Instance():
        return SceneConnection.instance

    # ------------------------------------------------------------------------------------------------------------------

    def InitNode(self):
        
        MaxPlus.Core.EvalMAXScript("""
        
        assetPubMainMod_def = attributes assetPubMainMod
        (
            parameters main rollout:params(
                assetData type:#string
            )
            rollout params "RealtimeUK - AssetPubTool"(label label01 "This holder contains useful data.")
        )

        obj = $RUK_AssetPubTool

        if obj == undefined do (
            obj = Circle()
            obj.name = "RUK_AssetPubTool"

            -- create new layer
            
            rukLayer = LayerManager.getLayerFromName ("RUK")

            if rukLayer == undefined then (
                    rukLayer = LayerManager.newLayer()
                    rukLayer.setname "RUK"
            )
            
            rukLayer.addnode obj

            freeze obj
            hide obj
        )

        try(
            attr = CustAttributes.Get obj.modifiers[1] 1
        )
        catch(
        
            eM=emptyModifier()
            em.name = "RUK_assetPubMainMod"
            
            custAttributes.add eM assetPubMainMod_def #unique
            addModifier obj eM
            attr = CustAttributes.Get eM 1

            attr.assetData = ""
        )
         
        """)

    def ForceResetNode(self):

        MaxPlus.Core.EvalMAXScript("""
                obj = $RUK_AssetPubTool
                delete obj
        """)

        self.InitNode()

    def ReadFromNode(self, field):

        self.InitNode()

        fpValue = MaxPlus.Core.EvalMAXScript(format(r"""
            fn GetData = (
                obj = $RUK_AssetPubTool
                attr = CustAttributes.Get obj.modifiers[1] 1						
                return attr.%s
            )
            
            GetData()
        """ %field))

        return fpValue.Get()

    def WriteToNode(self, jstring, field):

        self.InitNode()

        MaxPlus.Core.EvalMAXScript(format(r"""
            obj = $RUK_AssetPubTool
            attr = CustAttributes.Get obj.modifiers[1] 1						
            attr.%s = "%s"
        """ %(field, jstring)))