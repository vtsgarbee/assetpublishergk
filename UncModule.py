import subprocess
import pathlib

class UncHandler(object):

    inst = None

    @staticmethod
    def instance():

        if UncHandler.inst is None:
            UncHandler.inst = UncHandler()

        return UncHandler.inst

    def __init__(self):

        UncHandler.inst = self

        try:
            self.driveMappings = {}

            cmd = 'powershell.exe "Get-PSDrive | Select-Object -Property root, displayroot"'
            res = subprocess.check_output(cmd, shell=True)

            res = res.replace('\r', '\n')
            res = res.replace(' ', '')
            res = res.split('\n')

            for r in res:

                res = r.split(':\\')

                if len(res) == 2:

                    letter = res[0].lower()
                    displayRoot = res[1].lower()

                    if len(displayRoot) > 0:
                        self.driveMappings[letter] = displayRoot

        except Exception as e:
            print "Something went wrong while retrieving mapped drives:"
            print str(e)

            self.driveMappings = {}

    def convertToUnc(self, path):

        cPath = pathlib.Path(path)
        drive = cPath.drive.lower()
        drive = drive.replace(':', '')

        if drive in self.driveMappings:
            uncRoot = self.driveMappings[drive]
            uncRoot = pathlib.Path(uncRoot)
            leaf = str(cPath)[2:]

            return uncRoot.joinpath(leaf)

        return None