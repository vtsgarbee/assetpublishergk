import pymxs
import re
import UncModule
import AP_HelperFunctions

rt = pymxs.runtime

# ----------------------------------------------------------------------------------------------------------------------

# CHECKS RETURN VALUE
# True: everything ok
# False None: not ok (more info stored in c.info())

# CHECKS DEFINITION
# Priorities:
#   0 message
#   1 warning
#   2 error

# ----------------------------------------------------------------------------------------------------------------------

class CheckPack(object):

    code = None
    msg = "No error message set up"
    data = None
    priority = 0
    category = "Generic"

    def check(self, params):
        print "No check function available"
        return None

    def fix(self):
        print "No fix function available"
        return

    def info(self):
        return "No details available"

# NODE CHECKS ----------------------


class CheckNodeCount(CheckPack):

    def __init__(self):
        super(type(self), self).__init__()
        self.code = "NO_NODES"
        self.msg = "No nodes found"
        self.priority = 2
        self.category = "Nodes"

    def check(self, nodes):
        if len(nodes) == 0:
            return False

        return True

    def info(self):
        return "It looks like the !PUBLISHED layer is empty or doesn't exist"


class CheckSuffixes(CheckPack):

    def __init__(self):
        super(type(self), self).__init__()
        self.code = "NO_NUMBER_SUFFIX"
        self.msg = "All the node names should end with _001"
        self.priority = 1
        self.data = []
        self.category = "Nodes"

    def check(self, nodes):

        offendingNodes = []

        for n in nodes:

            if re.search("_001$", n.name) is None:
                offendingNodes.append(n.name)

        if len(offendingNodes) > 0:
            self.data = offendingNodes
            return False
        else:
            return True

    def info(self):
        nodesString = ",".join(self.data)
        return "Problematic nodes: " + nodesString


class CheckModifierStacks(CheckPack):

    def __init__(self):
        super(type(self), self).__init__()
        self.code = "MODIFIERS_NOT_COLLAPSED"
        self.msg = "Some modifier stacks have not been collapsed"
        self.priority = 1
        self.data = []
        self.category = "Nodes"

    def check(self, nodes):

        offendingNodes = []

        for n in nodes:
            for m in n.modifiers:
                c = rt.classOf(m)

                if c == rt.TurboSmooth or c == rt.OpenSubdiv or c == rt.Displace or c == rt.VRayDisplacementMod:
                    continue
                else:
                    offendingNodes.append(n.name)

        if len(offendingNodes) > 0:
            self.data = offendingNodes
            return False
        else:
            return True

    def info(self):
        nodesString = ",".join(self.data)
        return "Problematic nodes: " + nodesString


# MATERIAL CHECKS ---------------------


class CheckGammas(CheckPack):

    def __init__(self):
        super(type(self), self).__init__()
        self.code = "TEXTURE_GAMMAS"
        self.msg = "Some textures were loaded with incorrect gammas"
        self.priority = 1
        self.data = []
        self.category = "Materials"

    def check(self, materials):

        offendingMaterials = {}

        for m in materials:

            c = rt.classOf(m)

            if c == rt.vrayMtl:

                offendingTextures = []

                diff = m.texmap_diffuse

                if diff is not None:
                    if diff.color_space != 2:
                        offendingTextures.append(diff.name)

                if len(offendingTextures) > 0:
                    offendingMaterials[m.name] = offendingTextures

        if len(offendingMaterials) > 0:
            self.data = offendingMaterials
            return False
        else:
            return True

    def info(self):

        subStrings = []

        for mCode in self.data:

            txListReadable = ",".join(self.data[mCode])

            subString = "%s (%s)" % (mCode, txListReadable)
            subString = subString

            subStrings.append(subString)

        nodesString = ",".join(subStrings)
        return "Problematic nodes: " + nodesString


# TEXTURE CHECKS ----------------------


class CheckBitmaps(CheckPack):

    def __init__(self):
        super(type(self), self).__init__()
        self.code = "TEXTURES_BITMAPS"
        self.msg = "Some textures are loaded as 3dsmax bitmaps"
        self.priority = 1
        self.data = []
        self.category = "Textures"

    def check(self, textures):

        offendingNodes = []

        for t in textures:

            c = rt.classOf(t)

            if c == rt.Bitmaptexture:
                offendingNodes.append(t.name)

        if len(offendingNodes) > 0:
            self.data = offendingNodes
            return False
        else:
            return True

    def info(self):
        nodesString = ",".join(self.data)
        return "Problematic nodes: " + nodesString


class CheckLocalPaths(CheckPack):

    def __init__(self):
        super(type(self), self).__init__()
        self.code = "LOCAL_PATHS"
        self.msg = "Some textures were loaded from a local path, or their path is undefined"
        self.priority = 1
        self.data = []
        self.category = "Textures"

    def check(self, textures):

        offendingNodes = []

        for t in textures:

            if rt.classOf(t) == rt.VrayHdri:
                path = t.HDRIMapName

            elif rt.classOf(t) == rt.Bitmaptexture:
                path = t.fileName

            else:
                continue

            if path is None:
                offendingNodes.append(t.name)
                continue

            res = UncModule.UncHandler.instance().convertToUnc(path)

            if res is None:
                offendingNodes.append(t.name)

        if len(offendingNodes) > 0:
            self.data = offendingNodes
            return False
        else:
            return True

    def info(self):
        nodesString = ",".join(self.data)
        return "Problematic nodes: " + nodesString



# SANITIZER -----------------------------------------------------------------------------------------------------------

class Sanitizer(object):

    sInstance = None

    @staticmethod
    def instance():

        if Sanitizer.sInstance is None:
            Sanitizer.sInstance = Sanitizer()

        return Sanitizer.sInstance

    def __init__(self):

        self.nodesChecklist = [CheckNodeCount(), CheckModifierStacks(), CheckSuffixes()]
        self.texturesChecklist = [CheckBitmaps(), CheckLocalPaths()]
        self.materialsCheckList = [CheckGammas()]
        # self.materialsCheckList =

    def runChecks(self):

        activeWarnings = {}

        # COLLECTING NODES, MATERIALS, TEXTURES

        print "Collecting nodes..."

        nodes = AP_HelperFunctions.recursiveGetLayerNodes("!PUBLISHED")

        textures, materials, other = AP_HelperFunctions.getDependenciesByType(nodes)

        print "Checking nodes..."

        for c in self.nodesChecklist:

            if not c.check(nodes):
                activeWarnings[c.code] = c

        print "Checking textures..."

        for c in self.texturesChecklist:

            if not c.check(textures):
                activeWarnings[c.code] = c

        # print "Checking materials..."
        #
        # for c in self.materialsCheckList:
        #
        #     if not c.check(materials):
        #         activeWarnings[c.code] = c

        return activeWarnings